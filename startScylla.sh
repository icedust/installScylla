#!/bin/bash
if [ ! -d "scylla" ]; then
    echo 'make directory "scylla"'
    sudo mkdir scylla
fi

if [ ! -e "master_scylla.yaml" ]; then
    sudo wget https://gitlab.com/icedust/installScylla/raw/master/master_scylla.yaml
fi

if [ ! -e "lechain.pem" ]; then
    sudo wget https://gitlab.com/icedust/installScylla/raw/master/lechain.pem
fi

sudo docker run --name scylla_node --volume $(pwd)/master_scylla.yaml:/etc/scylla/scylla.yaml --volume $(pwd)/scylla:/var/lib/scylla -p 9042:9042 -p 7000:7000 -p 7001:7001 -p 7199:7199 -p 9160:9160 -p 9180:9180 -p 10000:10000 -d scylladb/scylla