from cassandra.cluster import Cluster
cluster = Cluster(['localhost'], port=9042)
session = cluster.connect()
session.execute('''
CREATE KEYSPACE Pathology WITH REPLICATION = {
    'class': 'NetworkTopologyStrategy',
    'datacenter1': 3
};
''')

session.execute('use pathology')
session.execute('''
CREATE TABLE slice_images(
    medical_record_id text,
    sample_id int,
    stain text,
    recut_id int,
    owner text,
    magnification int,
    report text,
    upload_status text,
    file_name uuid,
    algorithm_status text,
    update_time timestamp,
    PRIMARY KEY(
        medical_record_id,
        stain,
        sample_id,
        recut_id
    )
);
''')

session.execute('''
CREATE TABLE slice_images_history(
    medical_record_id text,
    sample_id int,
    stain text,
    recut_id int,
    user_id text,
    operation text,
    operation_time timestamp,
    PRIMARY KEY(
        medical_record_id,
        sample_id,
        stain,
        recut_id,
        user_id,
        operation,
        operation_time
    )
);
''')